# Configuration file

general {
    # Whether or not to allow RocksMC to be used as the default world format when no others are available.
    B:allowDefault=true

    ##########################################################################################################
    # database
    #--------------------------------------------------------------------------------------------------------#
    # Configuration options used for opening the database.
    # Generally speaking, the default options should be fine for virtually every user, and changing them can have serious implications for performance
    # and/or disk usage. Do not touch unless you know what you're doing!
    ##########################################################################################################

    database {
        # Whether or not to hint to the operating system that access to SST files will be random.
        # Default: true
        B:adviseRandom=true

        # Whether or not to allow multiple threads to write to the memtable at once.
        # Default: true
        B:allowConcurrentMemtableWrite=true

        # log2() of the number of entries to split the uncompressed data cache into.
        # Decreasing this will reduce the cache size (if the cache size is automatic), but may hurt performance.
        # Default: 6
        # Min: 1
        # Max: 2147483647
        I:cacheShardBits=6

        # The size of the uncompressed data cache (in MiB).
        # If 0, this will be determined automatically.
        # Default: 0
        # Min: 0
        # Max: 2147483647
        I:cacheSize=0

        # The compression algorithm to be used for compressing SST files.
        # Note that changing this will only affect newly created SSTs. Data already stored in the database will remain unchanged until
        # overwritten or included in a subsequent database compaction.
        # Warning: do not use DISABLE_COMPRESSION_OPTION! If you want to disable compression (e.g. because you're using filesystem-level compression), use NO_COMPRESSION.
        # Default: ZSTD_COMPRESSION
        # Valid values:
        # NO_COMPRESSION
        # SNAPPY_COMPRESSION
        # ZLIB_COMPRESSION
        # BZLIB2_COMPRESSION
        # LZ4_COMPRESSION
        # LZ4HC_COMPRESSION
        # XPRESS_COMPRESSION
        # ZSTD_COMPRESSION
        # DISABLE_COMPRESSION_OPTION
        S:compression=ZSTD_COMPRESSION

        # The target size for uncompressed data blocks (in KiB).
        # Larger values will yield a better data compression ratio at the cost of increased memory requirements for the cache.
        # Default: 1MiB (1024 KiB)
        # Min: 1
        # Max: 2147483647
        I:dataBlockSize=1024

        # Whether or not to use direct I/O for reads, bypassing the operating system's disk cache.
        # Default: false
        B:directReads=false

        # Whether or not to use direct I/O for writes, bypassing the operating system's disk cache.
        # Default: false
        B:directWrites=false

        # The number of threads to use when opening SST files.
        # Only has an effect if maxOpenFiles=-1.
        # Default: 16
        # Min: 1
        # Max: 2147483647
        I:fileOpeningThreads=16

        # If false, the WAL will be automatically flushed to disk after every write.
        # Default: false
        B:manualWalFlush=false

        # The maximum number of concurrent background jobs (both flushes and compactions combined).
        # If set to 0, the value of 'parallelism' will be used.
        # Default: 2
        # Min: 0
        # Max: 2147483647
        I:maxBackgroundJobs=0

        # The maximum number of files that may be open at once.
        # A value of -1 is equivalent to no limit.
        # Default: -1
        I:maxOpenFiles=-1

        # The maximum number of threads that will concurrently perform a compaction job by breaking it into multiple, smaller ones that are run simultaneously.
        # Default: 1 (no subcompactions)
        # Min: 1
        # Max: 2147483647
        I:maxSubcompactions=1

        # The maximum number of write buffers to mantain at once.
        # Having this set to a high number allows further writes to occur while filled write buffers are being compacted in the background.
        # If set to 0, the value of 'parallelism' will be used.
        # Default: 0
        # Min: 0
        # Max: 2147483647
        I:maxWriteBufferNumber=0

        # The maximum number of write buffers to merge into a single L0 table during compaction.
        # If set to 0, the value of 'parallelism' will be used.
        # Default: 0
        # Min: 0
        # Max: 2147483647
        I:minWriteBufferNumberToMerge=0

        # Whether or not to use memory-mapped I/O for reads.
        # Default: true
        B:mmapReads=true

        # Whether or not to use memory-mapped I/O for writes.
        # Default: true
        B:mmapWrites=true

        # The number of threads to use for flush and compactions.
        # Default: CPU count
        # Min: 1
        # Max: 2147483647
        I:parallelism=4

        # Whether or not to do paranoid validation of checksums.
        # Default: false
        B:paranoidChecks=false

        # If true, we won't update the statistics used to optimize compaction decisions by loading table properties from many SST files.
        # Turning this on will improve initial world load times for huge worlds, especially when running on slow storage.
        # Default: false
        B:skipStatsUpdateOnDbOpen=false

        # The target size for level-1 SST files (in KiB).
        # Default: 64MiB (65536KiB)
        # Min: 1
        # Max: 2147483647
        I:tableSizeBase=65536

        # The target file size multiplier for SST files at levels 2 and above.
        # For example, using tableSizeBase=1MiB and tableSizeMultiplier=2, level-1 SSTs will be ~1MiB, level-2 SSTs ~2MiB, level-3 SSTs ~4MiB, and so on.
        # Default: 1 (SSTs are the same size at all levels)
        # Min: 1
        # Max: 2147483647
        I:tableSizeMultiplier=1

        # If true, writing to SSTs will issue an fsync instead of an fdatasync.
        # Should only be used if using a filesystem such as ext3 which can lose files during a crash.
        # Default: false
        B:useFsync=false

        # The number of bytes to store in the write buffer before flushing to disk (in KiB).
        # If 0, write buffering will be disabled.
        # Default: 0
        # Min: 0
        # Max: 2147483647
        I:writeBufferSize=0
    }

}



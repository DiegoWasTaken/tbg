import mods.spawntabletweaker;

// Glowstone
recipes.addShaped("NN_Glowstone", <minecraft:glowstone_dust> * 4, [
  [<minecraft:gunpowder>, <minecraft:redstone>, <minecraft:gunpowder>],
  [<minecraft:redstone>, <minecraft:gunpowder>, <minecraft:redstone>],
  [<minecraft:gunpowder>, <minecraft:redstone>, <minecraft:gunpowder>]
]);

// Blaze items
furnace.addRecipe(<minecraft:blaze_powder>, <minecraft:glowstone_dust>, 0.3);
/* recipes.addShaped("NN_BlazePowder", <minecraft:blaze_powder> * 6, [
  [<minecraft:gunpowder>, <minecraft:gunpowder>, <minecraft:gunpowder>],
  [<minecraft:gunpowder>, <minecraft:flint_and_steel>, <minecraft:gunpowder>],
  [<minecraft:gunpowder>, <minecraft:gunpowder>, <minecraft:gunpowder>]
]); */
recipes.addShapeless("NN_BlazeRod", <minecraft:blaze_rod>, [<minecraft:bone>, <minecraft:blaze_powder> * 2]);

// Nether bricks
furnace.addRecipe(<minecraft:netherbrick>, <minecraft:brick>, 0.15);

// Nether wart and soul sand (for potions). May change how these work, I don't like this very much.
vanilla.seeds.addSeed(<minecraft:nether_wart> % 0.5);
recipes.addShapeless("NN_SoulSand", <minecraft:soul_sand>, [<minecraft:dirt>, <minecraft:sand>, <minecraft:dye:15>]);

// Wither skeletons
spawntabletweaker.addSpawnTagsBlacklist("minecraft:wither_skeleton", 8, 1, 1, "MONSTER", ["NETHER","END"]);